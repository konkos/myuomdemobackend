package gr.uom.myuomdemo;

import gr.uom.myuomdemo.course.Course;
import gr.uom.myuomdemo.course.CourseRepository;
import gr.uom.myuomdemo.department.Department;
import gr.uom.myuomdemo.department.DepartmentRepository;
import gr.uom.myuomdemo.professor.ProfessorRepository;
import gr.uom.myuomdemo.security.auth.AuthenticationResponse;
import gr.uom.myuomdemo.security.auth.RegisterProfessorRequest;
import gr.uom.myuomdemo.security.auth.RegisterStudentRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Random;

@Component
@RequiredArgsConstructor
@Slf4j
public class InitRunner implements CommandLineRunner {
    private final ProfessorRepository professorRepository;
    private final CourseRepository courseRepository;
    private final DepartmentRepository departmentRepository;

    private final RestTemplate restTemplate;

    @Override
    public void run(String... args) {
        createDepartment();
        addCourses();
        addProfessors();
        addStudents();
        createSchedule();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void addStudents() {
        List<Department> allDepartments = departmentRepository.findAll();

        for (int i = 0; i < 20; i++) {
            Department department = allDepartments.get(new Random().nextInt(allDepartments.size()));
            RegisterStudentRequest registerStudentRequest = RegisterStudentRequest.builder()
                    .firstName("firstName " + i)
                    .lastName("lastName " + i)
                    .email("stu" + i + "@uom.edu.gr")
                    .password("1234")
                    .registryNumber("ICS " + i)
                    .departmentName(department.getName())
                    .build();

            ResponseEntity<AuthenticationResponse> response = restTemplate
                    .postForEntity("http://localhost:8081/api/v1/auth/register/student",
                            registerStudentRequest,
                            AuthenticationResponse.class);

        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void createSchedule() {

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void createDepartment() {
        if (departmentRepository.count() != 0) return;
        for (int i = 1; i < 5; i++) {
            Department department = new Department();
            department.setUrl("url " + i);
            department.setName("dep " + i);
            departmentRepository.save(department);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void addCourses() {
        if (courseRepository.count() != 0) return;

        for (int i = 1; i < 20; i++) {
            Course course = Course.builder()
                    .name("name " + i)
                    .code("ICS " + i)
                    .description("Περιγραφή Μαθήματος")
                    .semester(new Random().nextInt(1, 9))
                    .isMandatory(true)
                    .ects(5)
                    .build();
            courseRepository.save(course);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void addProfessors() {
//        if (professorRepository.count() != 0) return;
        List<Department> all = departmentRepository.findAll();
        for (int i = 1; i < 20; i++) {
            int randomDepartment = new Random().nextInt(all.size());

            RegisterProfessorRequest registerProfessorRequest = RegisterProfessorRequest.builder()
                    .firstName("fname " + i)
                    .lastName("lname " + i)
                    .email("prof" + i + "@uom.edu.gr")
                    .password("1234")
                    .imgUrl("https://www.uom.gr/assets/site/public/nodes/1525/1173-achat-2.jpg")
                    .title("TITLE")
                    .tel("2310123456")
                    .building("Building ")
                    .office("office " + i)
                    .departmentName(all.get(randomDepartment).getName())
                    .build();

            ResponseEntity<AuthenticationResponse> response = restTemplate
                    .postForEntity("http://localhost:8081/api/v1/auth/register/professor",
                            registerProfessorRequest,
                            AuthenticationResponse.class);

        }
    }
}
