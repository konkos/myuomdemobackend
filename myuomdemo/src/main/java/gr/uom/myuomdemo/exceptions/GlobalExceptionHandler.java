package gr.uom.myuomdemo.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(ItemNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, String> itemNotFound(ItemNotFoundException e) {
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("error", e.getMessage());
        errorMap.put("code", String.valueOf(HttpStatus.NOT_FOUND));
        return errorMap;
    }

    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String, String> genericErrorHandler(Exception e) {
        log.error("STATUS 500 " + e.getMessage());
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("error", "Unspecified Error");
        errorMap.put("code", String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
        return errorMap;
    }
}
