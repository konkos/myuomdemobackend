package gr.uom.myuomdemo.schedule;

public enum DayOfTheWeekGR {
    MONDAY("ΔΕΥΤΕΡΑ"),
    TUESDAY("ΤΡΙΤΗ"),
    WEDNESDAY("ΤΕΤΑΡΤΗ"),
    THURSDAY("ΠΕΜΠΤΗ"),
    FRIDAY("ΠΑΡΑΣΚΕΥΗ"),
    SATURDAY("ΣΑΒΒΑΤΟ"),
    SUNDAY("ΚΥΡΙΑΚΗ");

    DayOfTheWeekGR(String day) {
    }
}
