package gr.uom.myuomdemo.restaurant;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Data
public class RestaurantDTO {

    private String day;
    private List<String> gevmaKirios;
    private String gevmaEidiko;
    private String gevmaGarnitoura;
    private String gevmaSalata;
    private String gevmaEpidorpio;
    private List<String> deipnoKirios;
    private String deipnoEidiko;
    private String deipnoGarnitoura;
    private String deipnoSalata;
    private String deipnoEpidorpio;
}