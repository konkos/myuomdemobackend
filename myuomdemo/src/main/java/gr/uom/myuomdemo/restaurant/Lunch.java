package gr.uom.myuomdemo.restaurant;

import gr.uom.myuomdemo.utils.StringListConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Lunch {

    @Column(name = "lunch_main")
    @Convert(converter = StringListConverter.class)
    private List<String> kirios;
    @Column(name = "lunch_special")
    private String eidiko;
    @Column(name = "lunch_garnish")
    private String garnitoura;
    @Column(name = "lunch_salad")
    private String salata;
    @Column(name = "lunch_dessert")
    private String epidorpio;


}