package gr.uom.myuomdemo.restaurant;

import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import gr.uom.myuomdemo.schedule.DayOfTheWeekGR;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RestaurantService {

    private final RestaurantRepository restaurantRepository;

    public RestaurantService(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    public List<Restaurant> getAllMeals() {
        return restaurantRepository.findAll();
    }

    public Restaurant getMealsOfTheDay(String day) {
        return restaurantRepository.findByDay(day).orElseThrow(() -> new ItemNotFoundException("Restaurant Not Found"));
    }

    public Restaurant addMeal(RestaurantDTO restaurantDTO) {
        String day = restaurantDTO.getDay();

        String deipnoEidiko = restaurantDTO.getDeipnoEidiko();
        String deipnoEpidorpio = restaurantDTO.getDeipnoEpidorpio();
        String deipnoGarnitoura = restaurantDTO.getDeipnoGarnitoura();
        List<String> deipnoKirios = restaurantDTO.getDeipnoKirios();
        String deipnoSalata = restaurantDTO.getDeipnoSalata();

        Dinner dinner = Dinner.builder()
                .kirios(deipnoKirios)
                .eidiko(deipnoEidiko)
                .garnitoura(deipnoGarnitoura)
                .salata(deipnoSalata)
                .epidorpio(deipnoEpidorpio)
                .build();

        String gevmaEpidorpio = restaurantDTO.getGevmaEpidorpio();
        String gevmaGarnitoura = restaurantDTO.getGevmaGarnitoura();
        List<String> gevmaKirios = restaurantDTO.getGevmaKirios();
        String gevmaSalata = restaurantDTO.getGevmaSalata();
        String gevmaEidiko = restaurantDTO.getGevmaEidiko();

        Lunch lunch = Lunch.builder()
                .kirios(gevmaKirios)
                .eidiko(gevmaEidiko)
                .garnitoura(gevmaGarnitoura)
                .salata(gevmaSalata)
                .epidorpio(gevmaEpidorpio)
                .build();

        Restaurant restaurant = Restaurant.builder()
                .day(DayOfTheWeekGR.valueOf(day))
                .lunch(lunch)
                .dinner(dinner)
                .build();

        return restaurantRepository.save(restaurant);
    }

    public void deleteMealByDay(String day) {
        restaurantRepository.deleteByDay(day);
    }
}