package gr.uom.myuomdemo.restaurant;

import gr.uom.myuomdemo.schedule.DayOfTheWeekGR;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    private DayOfTheWeekGR day;

    @Embedded
    private Lunch lunch;

    @Embedded
    private Dinner dinner;

}
