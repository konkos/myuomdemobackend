package gr.uom.myuomdemo.restaurant;

import gr.uom.myuomdemo.utils.StringListConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Dinner {

    @Column(name = "dinner_main")
    @Convert(converter = StringListConverter.class)
    private List<String> kirios;
    @Column(name = "dinner_special")
    private String eidiko;
    @Column(name = "dinner_garnish")
    private String garnitoura;
    @Column(name = "dinner_salad")
    private String salata;
    @Column(name = "dinner_dessert")
    private String epidorpio;
}