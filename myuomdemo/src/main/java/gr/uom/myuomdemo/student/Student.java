package gr.uom.myuomdemo.student;

import gr.uom.myuomdemo.department.Department;
import gr.uom.myuomdemo.registration.Registration;
import gr.uom.myuomdemo.security.user.User;
import jakarta.persistence.*;
import lombok.*;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@ToString
@RequiredArgsConstructor
@Builder
@Table(name = "student",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"registryNumber"})
        }
)
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    private String registryNumber;

    @ToString.Exclude
    @ManyToOne()//optional = false
    @JoinColumn(name = "department_id")//, nullable = false
    private Department department;

    @OneToMany(mappedBy = "student", orphanRemoval = true)
    @ToString.Exclude
    private Set<Registration> registrations = new LinkedHashSet<>();

}
