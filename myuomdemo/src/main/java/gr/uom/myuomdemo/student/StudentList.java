package gr.uom.myuomdemo.student;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class StudentList {
    private List<Student> students;
}
