package gr.uom.myuomdemo.student;

import gr.uom.myuomdemo.department.Department;
import gr.uom.myuomdemo.department.DepartmentService;
import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import gr.uom.myuomdemo.security.user.User;
import gr.uom.myuomdemo.security.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class StudentService {
    private final StudentRepository studentRepository;
    private final DepartmentService departmentService;
    private final UserRepository userRepository;


    public StudentList getAllStudents(int pageNum, int pageSize) {
        List<Student> students = studentRepository.findAll(PageRequest.of(pageNum, pageSize)).toList();
        return new StudentList(students);
    }

    public Student addStudent(StudentDTO student) {
        Department department = departmentService.getDepartmentByName(student.getDepartmentName());
        String userEmail = student.getUserEmail();
        User user = userRepository.findByEmail(userEmail).orElseThrow(() -> new ItemNotFoundException("User Not Found"));
        String registryNumber = student.getRegistryNumber();

        Student studentToBeAdded = Student.builder()
                .department(department).registryNumber(registryNumber).user(user).build();
        return studentRepository.save(studentToBeAdded);
    }

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }


    public StudentList addManyStudents(List<Student> students) {
        return StudentList.builder().students(studentRepository.saveAll(students)).build();
    }

    public Student updateStudent(Student student) {
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User loggedInUser = userRepository.findByEmail(principal.getUsername()).orElseThrow(() -> new ItemNotFoundException("User Not Found"));
        // TODO implement Update Student

        return new Student();
    }

}
