package gr.uom.myuomdemo.student;

import gr.uom.myuomdemo.security.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findByRegistryNumber(String registryNumber);

    Optional<Student> findByUser(User user);

}