package gr.uom.myuomdemo.student;

import gr.uom.myuomdemo.registration.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;
    private final RegistrationService registrationService;

    @GetMapping("/all")
    public StudentList getAllStudents(@RequestParam(name = "size", defaultValue = "20") int pageSize, @RequestParam(name = "page", defaultValue = "1") int pageNum) {
        return studentService.getAllStudents(pageNum, pageSize);
    }

    //TODO Remove - Insertion happens via the AuthenticationController
    @PostMapping
    public Student addStudent(@RequestBody StudentDTO student) {
        return studentService.addStudent(student);
    }

    @PostMapping("/addMany")
    public StudentList addManyStudents(@RequestBody List<Student> students) {
        return studentService.addManyStudents(students);
    }

    @PutMapping
    public Student updateStudent(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }

    @PostMapping("/register/{courseID}")
    public void registerStudentToCourse(@PathVariable String courseID) {
        registrationService.registerStudentToCourse(courseID);
    }
}
