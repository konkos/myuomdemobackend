package gr.uom.myuomdemo.student;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@Data
public class StudentDTO {
    private String userEmail;
    private String registryNumber;
    private String departmentName;
}
