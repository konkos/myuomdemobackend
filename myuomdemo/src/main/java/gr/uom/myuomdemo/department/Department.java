package gr.uom.myuomdemo.department;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import gr.uom.myuomdemo.professor.Professor;
import gr.uom.myuomdemo.school.School;
import gr.uom.myuomdemo.student.Student;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Builder
@AllArgsConstructor
@Entity
@Table(name = "Department",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"name"})
        }
)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    private String name;

    private String url;

    @OneToMany(mappedBy = "department", orphanRemoval = true)
    @ToString.Exclude
    @JsonBackReference("department")
    private Set<Professor> professors = new LinkedHashSet<>();

    @OneToMany(mappedBy = "department", orphanRemoval = true)
    @ToString.Exclude
    @JsonBackReference("department")
    private Set<Student> students = new LinkedHashSet<>();

    @ManyToOne
    @JoinColumn(name = "school_id", unique = true)
    private School school;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Department that = (Department) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
