package gr.uom.myuomdemo.department;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class DepartmentAdvice {

    @ExceptionHandler(DepartmentNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, String> depNotFound(Exception exception) {
        HashMap<String, String> errorMap = new HashMap<>();
        errorMap.put("error", exception.getMessage());
        return errorMap;
    }
}