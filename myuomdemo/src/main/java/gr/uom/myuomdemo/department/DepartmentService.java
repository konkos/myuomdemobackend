package gr.uom.myuomdemo.department;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Transactional
    public List<Department> getAllDepartments() {
        return departmentRepository.findAll();
    }

    @Transactional
    public Department getDepartmentByName(String name) {
        Optional<Department> byName = departmentRepository.findByName(name);
        return byName.orElseThrow(DepartmentNotFound::new);
    }

    @Transactional
    public Department updateDepartment(Department updatedDepartment) {
        String name = updatedDepartment.getName();
        Optional<Department> byName = departmentRepository.findByName(name);
        Department department = byName.orElseThrow(DepartmentNotFound::new);

        department.setUrl(updatedDepartment.getUrl());
        return departmentRepository.save(department);
    }

    @Transactional
    public Department addDepartment(Department department) {
        return departmentRepository.save(department);
    }
}