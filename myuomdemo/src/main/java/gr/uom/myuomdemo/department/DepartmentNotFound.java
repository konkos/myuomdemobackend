package gr.uom.myuomdemo.department;

public class DepartmentNotFound extends RuntimeException {
    public DepartmentNotFound() {
        super("Department Not Found");
    }
}