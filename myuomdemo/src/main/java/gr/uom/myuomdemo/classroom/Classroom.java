package gr.uom.myuomdemo.classroom;

import gr.uom.myuomdemo.schedule.ScheduleItem;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Classroom {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;


    @Column(nullable = false)
    private int numberOfSeats;


    @Enumerated(EnumType.STRING)
    @Column(name = "room_type")
    private RoomType roomType;

    @OneToMany(mappedBy = "classroom", orphanRemoval = true)
    private Set<ScheduleItem> scheduleItems = new LinkedHashSet<>();

}
