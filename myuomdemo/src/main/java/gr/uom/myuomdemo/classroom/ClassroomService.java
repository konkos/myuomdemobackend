package gr.uom.myuomdemo.classroom;

import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ClassroomService {

    private final ClassroomRepository classroomRepository;

    public ClassroomService(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }

    @Transactional(readOnly = true)
    public ClassRoomList getAllClassrooms() {
        ClassRoomList classroom = new ClassRoomList();
        classroom.setClassrooms(classroomRepository.findAll());
        return classroom;
    }

    @Transactional
    public Classroom addClassroom(Classroom classroom) {
        return classroomRepository.save(classroom);
    }

    @Transactional
    public void deleteClassroomByName(String name) {
        Classroom classroom = classroomRepository.findByNameIgnoreCase(name)
                .orElseThrow(() -> new ItemNotFoundException(String.format("Classroom with name %s Not Found", name)));
        classroomRepository.delete(classroom);
    }
}
