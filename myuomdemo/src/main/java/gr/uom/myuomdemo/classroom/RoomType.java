package gr.uom.myuomdemo.classroom;

public enum RoomType {

    AMPHITHEATER("ΑΜΦ"),
    CLASSROOM("ΑΙΘ"),
    LAB("ΕΡΓ");

    private final String type;

    RoomType(String type) {
        this.type = type;
    }
}
