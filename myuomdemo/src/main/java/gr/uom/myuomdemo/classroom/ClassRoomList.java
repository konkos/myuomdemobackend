package gr.uom.myuomdemo.classroom;

import lombok.Data;

import java.util.List;

@Data
public class ClassRoomList {
    private List<Classroom> classrooms;
}
