package gr.uom.myuomdemo.classroom;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/classroom")
public class ClassroomController {

    private final ClassroomService classroomService;

    public ClassroomController(ClassroomService classroomService) {
        this.classroomService = classroomService;
    }

    @GetMapping
    public ClassRoomList getAllClassrooms() {
        return classroomService.getAllClassrooms();
    }

    @PostMapping
    public Classroom addClassroom(@RequestBody Classroom classroom) {
        return classroomService.addClassroom(classroom);
    }

    @DeleteMapping("/{name}")
    public void deleteClassroomByName(@PathVariable(name = "name") String classRoomName) {
        classroomService.deleteClassroomByName(classRoomName);
    }
}
