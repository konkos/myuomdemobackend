package gr.uom.myuomdemo.registration;

import gr.uom.myuomdemo.course.Course;
import gr.uom.myuomdemo.student.Student;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@ToString
@RequiredArgsConstructor
@Builder
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "UniqueStudentCourse", columnNames = {"student_id", "course_id"})
})
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id")
    private Student student;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "course_id")
    private Course course;

    @Min(1)
    @Max(10)
    private Integer grade;
}
