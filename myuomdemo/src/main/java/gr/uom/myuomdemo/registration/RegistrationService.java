package gr.uom.myuomdemo.registration;

import gr.uom.myuomdemo.course.Course;
import gr.uom.myuomdemo.course.CourseRepository;
import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import gr.uom.myuomdemo.security.user.Role;
import gr.uom.myuomdemo.security.user.User;
import gr.uom.myuomdemo.security.user.UserRepository;
import gr.uom.myuomdemo.student.Student;
import gr.uom.myuomdemo.student.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegistrationService {
    private final RegistrationRepository registrationRepository;
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;
    private final UserRepository userRepository;


    public Registration registerStudentToCourse(String courseCode) {

        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User loggedInUser = userRepository.findByEmail(principal.getUsername()).orElseThrow(() -> new ItemNotFoundException("User Not Found"));

        if (!loggedInUser.getRole().equals(Role.STUDENT)) throw new RuntimeException("Unauthorized access");

        Student loggedInStudent = studentRepository.findByUser(loggedInUser).orElseThrow(() -> new ItemNotFoundException("Student Not Found"));
        Course course = courseRepository.findByCode(courseCode).orElseThrow(() -> new ItemNotFoundException("Course Not Found"));

        Registration registration = Registration.builder().course(course).student(loggedInStudent).grade(1).build();
        return registrationRepository.save(registration);
    }
}
