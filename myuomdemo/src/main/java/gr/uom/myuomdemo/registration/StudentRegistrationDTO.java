package gr.uom.myuomdemo.registration;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@RequiredArgsConstructor
@Builder
public class StudentRegistrationDTO {

    private String studentRegistrationID;
    private String courseRegistrationID;
}
