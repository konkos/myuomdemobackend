package gr.uom.myuomdemo.school;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gr.uom.myuomdemo.department.Department;
import jakarta.persistence.*;
import lombok.*;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @JsonIgnore
    private Long id;

    private String name;

    @OneToMany(mappedBy = "school", orphanRemoval = true)
    private Set<Department> departments = new LinkedHashSet<>();

}