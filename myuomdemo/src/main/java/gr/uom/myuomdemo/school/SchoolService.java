package gr.uom.myuomdemo.school;

import gr.uom.myuomdemo.department.Department;
import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class SchoolService {
    private final SchoolRepository schoolRepository;

    public Set<Department> getSchoolDepartments(String schoolName) {
        School school = schoolRepository.findByName(schoolName).orElseThrow(() -> new ItemNotFoundException("School Not Found"));

        return school.getDepartments();
    }

    public Department getDepartment(String schoolName, String depName) {
        School school = schoolRepository.findByName(schoolName)
                .orElseThrow(() -> new ItemNotFoundException("School Not Found"));
        return schoolRepository.findDepartmentByNameAndSchool(depName, school)
                .orElseThrow(() -> new ItemNotFoundException("Department Not Found"));
    }
}
