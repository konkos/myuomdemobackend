package gr.uom.myuomdemo.school;

import gr.uom.myuomdemo.department.Department;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/school")
@RequiredArgsConstructor
public class SchoolController {

    private final SchoolService schoolService;

    @GetMapping("/{schoolName}")
    public Set<Department> getSchoolDepartments(@PathVariable String schoolName) {
        return schoolService.getSchoolDepartments(schoolName);
    }

    @GetMapping("/{schoolName}/{depName}")
    public Department getDepartment(@PathVariable String schoolName, @PathVariable String depName) {
        return schoolService.getDepartment(schoolName, depName);
    }
}
