package gr.uom.myuomdemo.school;

import gr.uom.myuomdemo.department.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface SchoolRepository extends JpaRepository<School, Long> {

    Optional<School> findByName(String name);

    @Query("SELECT d FROM Department d WHERE d.school = :school AND d.name = :name")
    Optional<Department> findDepartmentByNameAndSchool(@Param("name") String name, @Param("school") School school);

//    Optional<Department> findByNameAndDepartments_Name(String name, String name1);


//    Optional<Department> findDepartmentByNameAndDepartmentName(String name, String depName);
}
