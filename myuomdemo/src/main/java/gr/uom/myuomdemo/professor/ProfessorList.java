package gr.uom.myuomdemo.professor;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProfessorList {
    private List<Professor> professors;
}
