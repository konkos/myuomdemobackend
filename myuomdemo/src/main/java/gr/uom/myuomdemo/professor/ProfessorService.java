package gr.uom.myuomdemo.professor;

import gr.uom.myuomdemo.department.Department;
import gr.uom.myuomdemo.department.DepartmentService;
import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import gr.uom.myuomdemo.security.user.Role;
import gr.uom.myuomdemo.security.user.User;
import gr.uom.myuomdemo.security.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class ProfessorService {

    private final ProfessorRepository professorRepository;
    private final DepartmentService departmentService;
    private final UserRepository userRepository;


    public ProfessorService(ProfessorRepository professorRepository, DepartmentService departmentService,
                            UserRepository userRepository) {
        this.professorRepository = professorRepository;
        this.departmentService = departmentService;
        this.userRepository = userRepository;
    }

    @Transactional
    public ProfessorList getAllProfessors(int pageNum, int pageSize) {
        List<Professor> professors = professorRepository.findAll(PageRequest.of(pageNum, pageSize)).toList();
        professors.forEach(p -> log.debug(p.getOffice().toString()));
        return ProfessorList.builder().professors(professors).build();
    }


    @Transactional
    public List<Professor> getProfessorsByInput(String input) {
        return professorRepository.getProfessorsByInput(input.trim());
    }

    @Transactional
    public Professor addProfessor(ProfessorDTO professorDTO) {
        String fname = professorDTO.getFname();
        String lname = professorDTO.getLname();
        String building = professorDTO.getBuilding();
        String email = professorDTO.getEmail();
        String office = professorDTO.getOffice();
        String tel = professorDTO.getTel();
        String imgUrl = professorDTO.getImgUrl();
        String title = professorDTO.getTitle();
        String departmentName = professorDTO.getDepartmentName();

        Department department = departmentService.getDepartmentByName(departmentName);

        Professor professor = Professor.builder()
//                .firstName(fname).lastName(lname)
                .department(department)
                .title(title)
                .telephone(tel)
//                .email(email)
                .office(Office.builder().office(office).building(building).build())
                .imgUrl(imgUrl).build();

        return professorRepository.save(professor);
    }

    @Transactional
    public List<Professor> addManyProfessors(List<ProfessorDTO> professors) {
        professors.forEach(this::addProfessor);
        return professorRepository.findAll();
    }

    @Transactional
    public Professor updateProfessor(Professor professor) {
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User loggedInUser = userRepository.findByEmail(principal.getUsername())
                .orElseThrow(() -> new ItemNotFoundException("User Not Found"));

        if (!loggedInUser.getRole().equals(Role.PROFESSOR)) throw new RuntimeException("Unauthorized access");

        Professor professorDB = professorRepository.findByUser(loggedInUser)
                .orElseThrow(() -> new ItemNotFoundException("Professor Not Found "));

        //TODO finish update
        professorDB.setDepartment(professor.getDepartment() == null ? professorDB.getDepartment() : professor.getDepartment());

        return professorRepository.save(professorDB);
    }
}