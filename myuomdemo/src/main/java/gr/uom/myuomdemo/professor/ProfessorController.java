package gr.uom.myuomdemo.professor;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/professors")
public class ProfessorController {

    private final ProfessorService professorService;

    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @GetMapping("/all")
    public ProfessorList getAllProfessors(@RequestParam(name = "size", defaultValue = "20") int pageSize, @RequestParam(name = "page", defaultValue = "0") int pageNum) {
        return professorService.getAllProfessors(pageNum, pageSize);
    }

    @GetMapping
    public ProfessorList getProfessorsByInput(@RequestParam() String input) {
        return ProfessorList.builder().professors(professorService.getProfessorsByInput(input)).build();
    }

    @PostMapping
    public Professor addProfessor(@RequestBody ProfessorDTO professorDTO) {
        return professorService.addProfessor(professorDTO);
    }

    @PostMapping("/addMany")
    public List<Professor> addManyProfessors(@RequestBody List<ProfessorDTO> professors) {
        return professorService.addManyProfessors(professors);
    }

    @PutMapping
    public Professor updateProfessor(@RequestBody Professor professor) {
        return professorService.updateProfessor(professor);
    }
}