package gr.uom.myuomdemo.security.auth;

import gr.uom.myuomdemo.security.user.Role;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Slf4j
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/register/student")
    public ResponseEntity<AuthenticationResponse> registerStudent(@RequestBody RegisterStudentRequest request) {
        log.info(request.toString());
        return ResponseEntity.ok(authenticationService.registerStudent(request));
    }

    @PostMapping("/register/professor")
    public ResponseEntity<AuthenticationResponse> registerProfessor(@RequestBody RegisterProfessorRequest request) {
        log.info(request.toString());
        return ResponseEntity.ok(authenticationService.registerProfessor(request));
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }

}
