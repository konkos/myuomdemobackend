package gr.uom.myuomdemo.security.auth;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegisterProfessorRequest {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String imgUrl;
    private String title;
    private String tel;
    private String building;
    private String office;
    private String departmentName;
}
