package gr.uom.myuomdemo.security.securityconfig;

import gr.uom.myuomdemo.security.user.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().disable();

        http.authorizeHttpRequests(req ->
                req.requestMatchers("/api/v1/students/**").hasAuthority(Role.STUDENT.name()));

        http.authorizeHttpRequests(req ->
                req.requestMatchers("/api/v1/professors/**").hasAuthority(Role.PROFESSOR.name()));

        http.authorizeHttpRequests(req -> req.requestMatchers("/api/v1/auth/**").permitAll());

        http.authorizeHttpRequests((authorize) -> authorize
                .anyRequest().authenticated());

        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
