package gr.uom.myuomdemo.security.user;

public enum Role {

    USER("USER"),
    STUDENT("STUDENT"),
    PROFESSOR("PROFESSOR"),
    ADMIN("ADMIN"),
    GUEST("GUEST");

    private final String name;

    Role(String name) {
        this.name = name;
    }
}
