package gr.uom.myuomdemo.security.auth;


import gr.uom.myuomdemo.department.Department;
import gr.uom.myuomdemo.department.DepartmentService;
import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import gr.uom.myuomdemo.professor.Office;
import gr.uom.myuomdemo.professor.Professor;
import gr.uom.myuomdemo.professor.ProfessorRepository;
import gr.uom.myuomdemo.security.securityconfig.JWTService;
import gr.uom.myuomdemo.security.user.Role;
import gr.uom.myuomdemo.security.user.User;
import gr.uom.myuomdemo.security.user.UserRepository;
import gr.uom.myuomdemo.student.Student;
import gr.uom.myuomdemo.student.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class AuthenticationService {

    private final UserRepository userRepository;
    private final StudentService studentService;
    private final DepartmentService departmentService;
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;
    private final AuthenticationManager authenticationManager;
    private final ProfessorRepository professorRepository;

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        String email = request.getEmail();
        String password = request.getPassword();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));

        User user = userRepository.findByEmail(email).orElseThrow(() -> new ItemNotFoundException("User not Found"));
        String jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder().token(jwtToken).build();
    }

    public AuthenticationResponse register(RegisterRequest request, Role role) {
        User user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(role)
                .build();

        userRepository.save(user);
        String jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder().token(jwtToken).build();
    }

    public AuthenticationResponse registerStudent(RegisterStudentRequest request) {
        RegisterRequest build = RegisterRequest.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(request.getPassword())
                .build();
        AuthenticationResponse register = register(build, Role.STUDENT);

        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new ItemNotFoundException("User Not Found"));
        Department department = departmentService.getDepartmentByName(request.getDepartmentName());

        Student student = Student.builder()
                .user(user)
                .registryNumber(request.getRegistryNumber())
                .department(department)
                .build();

        studentService.addStudent(student);

        return register;
    }

    public AuthenticationResponse registerProfessor(RegisterProfessorRequest request) {
        RegisterRequest build = RegisterRequest.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(request.getPassword())
                .build();
        AuthenticationResponse register = register(build, Role.PROFESSOR);

        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new ItemNotFoundException("User Not Found"));
        Department department = departmentService.getDepartmentByName(request.getDepartmentName());

        Professor professor = Professor.builder()
                .imgUrl(request.getImgUrl())
                .title(request.getTitle())
                .office(Office.builder().building(request.getBuilding()).office(request.getOffice()).build())
                .telephone(request.getTel())
                .department(department)
                .user(user)
                .build();

        professorRepository.save(professor);

        return register;
    }
}
