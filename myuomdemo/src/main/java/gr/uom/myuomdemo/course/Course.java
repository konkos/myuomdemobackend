package gr.uom.myuomdemo.course;

import gr.uom.myuomdemo.registration.Registration;
import gr.uom.myuomdemo.schedule.ScheduleItem;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@ToString
@RequiredArgsConstructor
@Builder
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    @NotBlank
    private String name;

    @NotBlank
    private String code;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Min(1)
    @Max(8)
    private int semester;

    private Boolean isMandatory;

    @Column(name = "student_group")
    private String group;

    private int ects;

    @ToString.Exclude
    @OneToMany(mappedBy = "course", orphanRemoval = true)
    private Set<Registration> registrations = new LinkedHashSet<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "course", orphanRemoval = true)
    private Set<ScheduleItem> scheduleItems = new LinkedHashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Course course = (Course) o;
        return id != null && Objects.equals(id, course.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
