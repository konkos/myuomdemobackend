package gr.uom.myuomdemo.course;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/courses")
public class CourseController {

    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public CourseList getAllCourses(@RequestParam(name = "size") int pagesize, @RequestParam(name = "page") int pageNum) {
        return courseService.getAllCourses(pageNum, pagesize);
    }

    @PostMapping
    public Course addCourse(@RequestBody Course course) {
        return courseService.addCourse(course);
    }

    @PutMapping("/{id}")
    public Course modifyCourse(@PathVariable Long id, @RequestBody Course course) {
        return courseService.modifyCourse(id, course);
    }
}
