package gr.uom.myuomdemo.course;

import gr.uom.myuomdemo.exceptions.ItemNotFoundException;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CourseService {

    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Transactional(readOnly = true)
    public CourseList getAllCourses(int pageNum, int pagesize) {
        List<Course> courses = courseRepository.findAll(PageRequest.of(pageNum, pagesize)).toList();
        return new CourseList(courses);
    }

    @Transactional
    public Course addCourse(Course course) {
        return courseRepository.save(course);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = {ItemNotFoundException.class})
    public Course modifyCourse(Long id, Course course) {
        Course courseFound = courseRepository.findById(id).orElseThrow(() -> new ItemNotFoundException("Course Not Found"));

        courseFound.setCode(courseFound.getCode() == null ? course.getCode() : courseFound.getCode());
        courseFound.setName(courseFound.getName() == null ? course.getName() : courseFound.getName());
        courseFound.setDescription(courseFound.getDescription() == null ? course.getDescription() : courseFound.getDescription());

        courseRepository.save(courseFound);
        return courseFound;
    }
}
