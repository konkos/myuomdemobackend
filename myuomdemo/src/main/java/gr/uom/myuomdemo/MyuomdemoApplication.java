package gr.uom.myuomdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyuomdemoApplication {
    // https://github.com/Ozair0/Spring-Boot-3-Auth-JWT-Cookie-JPA
    public static void main(String[] args) {
        SpringApplication.run(MyuomdemoApplication.class, args);
    }

}
